var translations = {
  "title": {
    de: "Trockenstress",
    fr: "Stress hydrique",
    it: "Stress da siccità",
    en: "Drought stress"
  },
  "explanation" : {
    de: "Erklärung",
    fr: "Explication",
    it: "Spiegazione",
    en: "Explanation"
  },
  "map" : {
    de: "Interaktive Karte",
    fr: "Carte interactive",
    it: "Carta interattiva",
    en: "Interactive map"
  },
  "diagram" : {
    de: "Jahresdiagramm",
    fr: "Graphique annuel",
    it: "Diagramma annuale",
    en: "Year diagram"
  },
  "usedData" : {
    de: "<i class=\"fas fa-tint\"></i> Verwendete Daten",
    fr: "<i class=\"fas fa-tint\"></i> Données utilisées",
    it: "<i class=\"fas fa-tint\"></i> Dati utilizzati",
    en: "<i class=\"fas fa-tint\"></i> Data used"
  },
  "waterPlants" : {
    de: "Wasserverfügbarkeit für Pflanzen (Evapotranspiration aktuell vs. potenziell)",
    fr: "Eau disponible pour les plantes (évapotranspiration actuelle et potentielle)",
    it: "Disponibilità di acqua per le piante (evapotraspirazione attuale vs. potenziale)",
    en: "Water availability for plants (evapotranspiration current vs. potential)"
  },
  "waterGround" : {
    de: "Wasserverfügbarkeit im Boden (klimatische Wasserbilanz)",
    fr: "Eau disponible dans les sols (bilan hydrique climatique)",
    it: "Disponibilità di acqua nel suolo (bilancio idrico climatico)",
    en: "Water availability in the soil (climatic water balance)"
  },
  "explanationTitle" : {
    de: "<i class=\"fas fa-question\"></i> Erklärung",
    fr: "<i class=\"fas fa-question\"></i> Explication",
    it: "<i class=\"fas fa-question\"></i> Spiegazione",
    en: "<i class=\"fas fa-question\"></i> Explanation",
  },
  "waterPlantsTitle" : {
    de: "Wasserverfügbarkeit für Pflanzen",
    fr: "Eau disponible pour les plantes",
    it: "Disponibilità di acqua per le piante",
    en: "Water availability for plants"
  },
  "waterPlantsSubTitle" : {
    de: "Den Bäumen steht immer öfter zu wenig Wasser zur Verfügung",
    fr: "Les arbres disposent de toujours moins d’eau.",
    it: "Gli alberi hanno sempre più spesso una disponibilità d’acqua insufficiente",
    en: "Too little water is available to the trees more and more often."
  },
  "plantsExplanation1" : {
    de: "Die Vegetation und der Boden geben Wasser an die Atmosphäre ab, je wärmer und trockener die Luft, desto mehr. Pflanzen verdunsten über ihre Blätter Wasser. Die Evapotranspiration ist die Summe des an die Atmosphäre abgegebenen Wassers.",
    fr: "Les plantes et les sols rejettent de l’eau dans l’atmosphère ; plus l’air est chaud et sec, plus les volumes rejetés sont importants. L’eau s’évapore par les feuilles des plantes. L’évapotranspiration correspond à l’ensemble du volume d’eau rejeté dans l’atmosphère.",
    it: "La vegetazione e il suolo rilasciano acqua nell’atmosfera, in quantità più o meno elevata a seconda della temperatura e della secchezza dell’aria. L’acqua delle piante evapora attraverso le foglie. L’evapotraspirazione è la somma dell’acqua rilasciata nell’atmosfera.",
    en: "The vegetation and soil release water into the atmosphere, even moreso the warmer and drier the air. Plants evaporate water through their leaves. Evapotranspiration is the sum of the water released into the atmosphere."
  },
  "plantsExplanation2" : {
    de: "Die aktuelle Evapotranspiration (ETa) hängt von der Wasserverfügbarkeit ab, das heisst von der Niederschlagsmenge sowie der Fähigkeit der Böden, das Niederschlagswasser zu speichern. Die potenzielle Evapotranspiration (ETp) ist die Wassermenge, die bei ausreichender Wassernachlieferung an die Atmosphäre abgegeben würde.",
    fr: "L’évapotranspiration actuelle (ET) dépend de la disponibilité effective en eau (quantités de précipitations et capacité des sols à stocker ces dernières). L’évapotranspiration potentielle (ETP) correspond à la quantité d’eau rejetée dans l’atmosphère lorsque l’eau est disponible en quantité suffisante.",
    it: "L’evapotraspirazione attuale (ETa) dipende dalla disponibilità di acqua, ossia dalla quantità di precipitazioni e dalla capacità di assorbimento delle acque piovane da parte del suolo. L’evapotraspirazione potenziale (ETp) indica la quantità di acqua che sarebbe rilasciata nell’atmosfera in caso di approvvigionamento idrico sufficiente.",
    en: "The current evapotranspiration (ETa) depends on the availability of water, i.e. on the amount of precipitation and the capacity of the soil to store rainwater. The potential evapotranspiration (ETp) is the amount of water that would be released into the atmosphere if sufficient water were available."
  },
  "plantsExplanation3" : {
    de: "Unterhalb von einem ETa/ETp-Verhältnis von 0.8 ist mit Beeinträchtigungen durch Trockenheit zu rechnen, da die Spaltöffnungen in den Blättern dann oft geschlossen sind und die Bäume die Photosynthese einschränken.",
    fr: "Si le rapport ET/ETP est inférieur à 0,8, il faut s’attendre à des effets nuisibles dus à la sécheresse : les stomates restent souvent fermés, ce qui limite la photosynthèse.",
    it: "Un valore inferiore al rapporto ETa/ETp di 0,8 presume un danno da siccità poiché gli stomi delle foglie sono spesso chiusi, ciò che limita la fotosintesi degli alberi.",
    en: "Below an ETa/ETp ratio of 0.8, drought is likely to affect the plant, as the stomata in the leaves are often closed and the trees limit photosynthesis."
  },
  "plantsExplanation4" : {
    de: "In hell- und speziell dunkelblauen Gebieten ist nicht mit Störungen zu rechnen.",
    fr: "Dans les zones bleues (surtout bleu foncé), la situation n’est pas problématique.",
    it: "Nelle zone in celeste e, in particolare, in quelle in azzurro non sono presenti disturbi.",
    en: "In light and especially dark blue areas there are no disturbances to be expected."
  },
  "plantsExplanation5" : {
    de: "In gelb bis rot markierten Gebieten ist mit Beeinträchtigungen der Bäume durch Trockenheit zu rechnen, am stärksten in den weinroten Bereichen.",
    fr: "Dans les zones jaunes à rouges (surtout bordeaux), il faut s’attendre à des effets nuisibles dus à la sécheresse.",
    it: "Nelle zone da giallo a rosso, gli alberi sono danneggiati dalla siccità. I danni più gravi sono presenti nelle zone bordeaux.",
    en: "In areas marked yellow to red, the trees can be expected to be affected by drought, especially in the wine red areas."
  },
  "plantsExplanation6" : {
    de: "Trend: Wiederkehr und Ausdehnung gelb bis roter Gebiete nehmen in der Periode 1981-2018 zu.",
    fr: "Tendance : les zones jaunes à rouges s’étendent de plus en plus d’une année à l’autre (période de 1981 à 2018).",
    it: "Tendenza: la frequenza e l’estensione nelle zone da giallo a rosso aumentano nel periodo 1981-2018.",
    en: "Trend: Return and expansion of yellow to red areas increase in the period 1981-2018."
  },
  "dataExplanation" : {
    de: "Periode 1981-2018, April bis August, räumliche Auflösung 250m x 250m. Quelle: Remund et al. (2016).",
    fr: "Période 1981-2018, avril à août, résolution 250 m x 250 m. Source : Remund et al. (2016)",
    it: "Periodo 1981-2018, da aprile ad agosto, risoluzione geografica 250 x 250 m. Fonte: Remund et al. (2016).",
    en: "Period 1981-2018, April to August, spatial resolution 250m x 250m. Source: Remund et al. (2016)."
  },
  "waterGroundTitle" : {
    de: "Wasserverfügbarkeit im Boden",
    fr: "Eau disponible dans les sols",
    it: "Disponibilità di acqua nel suolo",
    en: "Water availability in the soil"
  },
  "waterGroundSubTitle" : {
    de: "Den Bäumen steht in der Vegetationszeit häufiger weniger Wasser zur Verfügung",
    fr: "Les arbres disposent toujours plus de moins en moins d’eau durant la période de végétation.",
    it: "Durante il periodo vegetativo, gli alberi hanno sempre più spesso una disponibilità d’acqua insufficiente",
    en: "The trees often have less water available during the vegetation period."
  },
  "waterGroundExplanation1" : {
    de: "Das Wasserangebot in der Vegetationszeit ist für Wachstum und Vitalität der Wälder entscheidend. Die Verfügbarkeit des Wassers kann aus der Differenz zwischen dem Niederschlag und der potenziellen Verdunstung berechnet werden. Dies ist die klimatische Wasserbilanz. Erweitert um den Bodenspeicher ergibt sich die Standortswasserbilanz.",
    fr: "Durant la période de végétation, l’eau joue un rôle crucial pour la croissance et la vitalité des arbres. La disponibilité en eau correspond à la différence entre les précipitations et l’évaporation potentielle. On parle alors de bilan hydrique climatique. Si l’on tient compte de la capacité de stockage des sols, on obtient le bilan hydrique de la station.",
    it: "La disponibilità di acqua nel periodo vegetativo è determinante per la crescita e la vitalità dei boschi. Essa può essere calcolata dalla differenza fra le precipitazioni e l’evaporazione potenziale, il cosiddetto bilancio idrico climatico. Aggiungendovi la capacità di assorbimento del suolo si ottiene il bilancio idrico della stazione.",
    en: "The water supply during the vegetation period is decisive for the growth and vitality of the forests. The availability of water can be calculated from the difference between precipitation and potential evaporation. This is the climatic water balance. If the soil reservoir is added, the site water balance is calculated."
  },
  "waterGroundExplanation2" : {
    de: "Die Standortswasserbilanz steht in enger Beziehung zum Vorkommen der Bäume, ihrer Vitalität und zum Wachstum.",
    fr: "Le bilan hydrique de la station dépend étroitement de la présence d’arbres, de leur vitalité et de leur croissance.",
    it: "Il bilancio idrico della stazione è in stretta correlazione con la presenza degli alberi, la loro vitalità e la crescita.",
    en: "The site water balance is closely related to the occurrence of trees, their vitality and growth."
  },
  "waterGroundExplanation3" : {
    de: "Die Baumarten haben unterschiedliche Ansprüche an das Wasserangebot. Während Eichen genügsam sind, brauchen Fichten deutlich mehr Wasser in der Vegetationszeit.",
    fr: "Le besoin en eau varie d’une essence à l’autre. Si les chênes sont peu exigeants, les épicéas sont nettement plus gourmands en eau durant la période de végétation.",
    it: "Le specie arboree hanno esigenze di disponibilità di acqua diverse. Se le querce sono parsimoniose, i pecci hanno bisogno di molta più acqua durante il periodo vegetativo.",
    en: "The tree species have different demands on the water supply. While oaks are frugal, spruce need considerably more water during the vegetation period."
  },
  "waterGroundExplanation4" : {
    de: "Trend: Wiederkehr und Ausdehnung gelb bis roter Gebiete nehmen in der Periode 1981-2018 zu.",
    fr: "Tendance : les zones jaunes à rouges s’étendent de plus en plus d’une année à l’autre (période de 1981 à 2018).",
    it: "Tendenza: la frequenza e l’estensione nelle zone da giallo a rosso aumentano nel periodo 1981-2018.",
    en: "Trend: Return and expansion of yellow to red areas increase in the period 1981-2018."
  },
  "legendTitle" : {
    de: "<i class=\"fas fa-palette\"></i> Legende",
    fr: "<i class=\"fas fa-palette\"></i> Légende",
    it: "<i class=\"fas fa-palette\"></i> Legenda",
    en: "<i class=\"fas fa-palette\"></i> Legend"
  },
  "yearTitle" : {
    de: "Jahr",
    fr: "Année",
    it: "Anno",
    en: "Year"
  },
  "yearDiagram" : {
    de: "<i class=\"fas fa-chart-area\"></i> Jahresdiagramm",
    fr: "<i class=\"fas fa-chart-area\"></i> Graphique annuel",
    it: "<i class=\"fas fa-chart-area\"></i> Diagramma annuale",
    en: "<i class=\"fas fa-chart-area\"></i> Year diagram"
  },
  "waterPlantsDiagramTitle" : {
    de: "Wasserverfügbarkeit für Pflanzen an den gewählten Standorten",
    fr: "Eau disponible pour les plantes dans les stations sélectionnées",
    it: "Disponibilità di acqua per le piante in stazioni selezionate",
    en: "Water availability for plants at the selected sites"
  },
  "waterGroundDiagramTitle" : {
    de: "Wasserverfügbarkeit im Boden an den gewählten Standorten",
    fr: "Eau disponible dans les sols des stations sélectionnées",
    it: "Disponibilità di acqua nel suolo in stazioni selezionate",
    en: "Water availability in the soil at the selected sites"
  },
  "shareLink" : {
    de: "<i class=\"fas fa-link\"></i> Link zum Teilen",
    fr: "<i class=\"fas fa-link\"></i> Lien",
    it: "<i class=\"fas fa-link\"></i> Link per condividere",
    en: "<i class=\"fas fa-link\"></i> Share"
  },
  "about" : {
    de: "Über die App",
    fr: "Via l’application",
    it: "Informazioni sull’app",
    en: "About the app"
  },
  "credit" : {
    de: "Diese Visualisierung wurde erstellt von Sandro Baselgia und Mirco Rothenfluh.",
    fr: "Ce contenu a été réalisé par M. Sandro Baselgia et M. Mirco Rothenfluh.",
    it: "Questa vista è stata allestita da Sandro Baselgia e Mirco Rothenfluh.",
    en: "This visualization was created by Sandro Baseliga and Mirco Rothenfluh"
  },
  "dataCredit" : {
    de: "Die Daten stammen vom Bundesamt für Umwelt und wurden von Thomas Bettler zur Verfügung gestellt.",
    fr: "Les données proviennent de l’Office fédéral de l’environnement et ont été fournies par M. Thomas Bettler.",
    it: "I dati provengono da Thomas Bettler, Ufficio federale dell’ambiente, che li ha messi a disposizione.",
    en: "The data originate from the Federal Office for the Environment and were provided by Thomas Bettler."
  },
  "links" : {
    de: "Weiterführende Links",
    fr: "Pour en savoir plus :",
    it: "Link di approfondimento",
    en: "Further Links"
  },
  "bafu" : {
    de: "Bundesamt für Umwelt",
    fr: "Office fédéral de l’environnement",
    it: "Ufficio federale dell’ambiente",
    en: "Federal Office for the Environment"
  },
  "iwi" : {
    de: "Institut für Wirtschaftsinformatik",
    fr: "Institut für Wirtschaftsinformatik",
    it: "Istituto di informatica di gestione",
    en: "Institute of Information Systems"
  },
  "fdg" : {
    de: "Forschungsstelle Digitale Nachhaltigkeit",
    fr: "Forschungsstelle Digitale Nachhaltigkeit",
    it: "Centro di ricerca sulla sostenibilità digitale",
    en: "Research Center for Digital Sustainability"
  },
  "fullscreenMode" : {
    de: "Präsentationsansicht Forstmesse",
    fr: "Présentation lors de la Foire forestière",
    it: "Vista di presentazione Fiera forestale",
    en: "Presentation mode"
  },
  "location" : {
    de: "Ausgewählter Standort",
    fr: "Station sélectionnée",
    it: "Stazione selezionata",
    en: "Selected location"
  },
  "compareLocation" : {
    de: "Standort vergleichen",
    fr: "Comparer les stations",
    it: "Comparare la stazione",
    en: "Compare location"
  },
  "newDiagram" : {
    de: "Neues Diagramm",
    fr: "Nouveau graphique",
    it: "Nuovo diagramma",
    en: "New diagram"
  },
} 
