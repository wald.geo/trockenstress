let chart;
let yearCount;
let autoInterval;
let activeAutoplay = false;
let wmtsSourceLayerURL = "ch.bafu.wald-wasserverfuegbarkeit_pflanzen";
let mapType = "pflanzen";
let wmtsSource;
let localityNames = [];
let map;
let pointLayer;
let lang = "de";
let translator;
let stacGround = {};
let stacPlants = {};
let geoTiffs;
const firstYear = 1981;

let globalYear = firstYear;
let globalMapX = 2630000;
let globalMapY = 1180000;
let globalZoom = 3.6;
let globalDataType = "etap";
let coordinatesClicked;

const resolutions = [
  4000, 3750, 3500, 3250, 3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250, 1000, 750, 650, 500, 250, 100, 50,
];
const extent = [2420000, 130000, 2900000, 1350000];

ol.proj.proj4.register(proj4);

/**
 * Get the number of years from the stac API
 */
function countYears() {
  yearCount =
    new Date(stacGround.extent.temporal.interval[0][1]).getFullYear() -
    new Date(stacGround.extent.temporal.interval[0][0]).getFullYear() +
    1;
}

/**
 * Generates the URL to a geoTIFF for a specific year with the currently selected mapType
 * @param {number} year the year for which the URL should be generated
 * @returns {string} the URL to the geoTIFF
 */
function geoTiffUrl(year) {
  return `https://data.geo.admin.ch/ch.bafu.wald-wasserverfuegbarkeit_${mapType}/wald-wasserverfuegbarkeit_${mapType}_${
    year || globalYear
  }/wald-wasserverfuegbarkeit_${mapType}_${year || globalYear}_2056.tif`;
}

function buildMap() {
  const projection = ol.proj.get("EPSG:2056");
  projection.setExtent(extent);

  const matrixIds = [];
  for (let i = 0; i < resolutions.length; i++) {
    matrixIds.push(i);
  }

  const tileGrid = new ol.tilegrid.WMTS({
    origin: [extent[0], extent[3]],
    resolutions: resolutions,
    matrixIds: matrixIds,
    maxZoom: 14,
    minZoom: 3,
  });

  wmtsSource = new ol.source.WMTS({
    url: "https://wmts.geo.admin.ch/1.0.0/{Layer}/default/{Time}/{TileMatrixSet}/{TileMatrix}/{TileCol}/{TileRow}.png",
    layer: wmtsSourceLayerURL,
    extension: "image/png",
    matrixSet: "2056",
    tileGrid: tileGrid,
    style: "default",
    dimensions: {
      Time: firstYear + yearCount - 1,
    },
    requestEncoding: "REST",
  });

  const backgroundLayer = new ol.layer.Tile({
    id: "background-layer",
    source: new ol.source.XYZ({
      url: `https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg`,
    }),
  });

  pointLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
      projection: "EPSG:2056",
    }),
  });

  map = new ol.Map({
    target: "map",
    layers: [
      backgroundLayer,
      new ol.layer.Tile({
        source: wmtsSource,
        opacity: 0.8,
      }),
      pointLayer,
    ],
    view: new ol.View({
      center: [globalMapX, globalMapY],
      projection: projection,
      zoom: globalZoom,
      maxZoom: 14,
      minZoom: 3,
    }),
    controls: ol.control
      .defaults({
        attributionOptions: {
          collapsible: false,
        },
      })
      .extend([
        new ol.control.ScaleLine({
          units: "metric",
        }),
        new ol.control.FullScreen(),
      ]),
  });

  map.on("singleclick", handleMapClick);

  map.on("moveend", function (evt) {
    pushURL();
  });

  updateYear(wmtsSource, globalYear);
  document.getElementById("yearSlider").addEventListener("input", function () {
    updateYear(wmtsSource, this.value);
  });
}

/**
 * Change the year of the heatmap layer, the year slider and the chart
 * @param {ol.source.wmts} source the source of the heatmap layer
 * @param {number} sliderVal the year to set
 */
function updateYear(source, sliderVal) {
  source.updateDimensions({ Time: sliderVal });
  document.getElementById("span_year").innerHTML = sliderVal;
  $("#yearSlider").val(sliderVal);

  if (chart != undefined) {
    chart.colorYearInChart(parseInt(sliderVal) - firstYear);
  }

  pushURL();
}

/**
 * Handles the click event on the map.
 * If there is no chart, create a new one.
 * If there is already a chart, show the modal to select whether to create a new chart
 * or add a data point to the existing one.
 * @param {Event} evt
 */
function handleMapClick(evt) {
  const coordinates = evt.coordinate;
  coordinatesClicked = [roundCoordinate(coordinates[0]), roundCoordinate(coordinates[1])];

  if (chart == undefined) {
    chart = new Chart(
      coordinatesClicked[0],
      coordinatesClicked[1],
      globalDataType,
      geoTiffs,
      firstYear,
      yearCount,
      changeYearInChart,
      displayPointsOnMap,
      pushURL
    );
    chart.build();
  } else {
    $("#modal_mapdata").modal("open");
  }
}

/**
 * 
 * @param {boolean} newChart whether to create a new chart or add a data point to the existing one
 */
function handleModalButtons(newChart) {
  if (newChart) {
    chart = new Chart(
      coordinatesClicked[0],
      coordinatesClicked[1],
      globalDataType,
      geoTiffs,
      firstYear,
      yearCount,
      changeYearInChart,
      displayPointsOnMap,
      pushURL
    );
    chart.build();
  } else {
    chart.addCoordinates(coordinatesClicked[0], coordinatesClicked[1]);
  }
}

/**
 * Clears the pointLayer of the maps and adds the new points.
 * Points and colors are linked using the array index of their respective arrays.
 * @param {Array<Array<number>>} points an array of points, which are an array holding two coordinates each
 * @param {Array<string>} colors an array of css colors of the format "rgb(xxx,xxx,xxx)"
 */
function displayPointsOnMap(points, colors) {
  const pointFeartures = [];

  const stroke = new ol.style.Stroke({
    color: "black",
    width: 1,
  });

  points.forEach(function (point, index) {
    const pointFeature = new ol.Feature(new ol.geom.Point(point));

    const fill = new ol.style.Fill({
      color: colors[index],
    });

    pointFeature.setStyle(
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: fill,
          stroke: stroke,
          radius: 5,
        }),
        fill: fill,
        stroke: stroke,
      })
    );

    pointFeartures.push(pointFeature);
  });

  pointLayer.getSource().clear();
  pointLayer.getSource().addFeatures(pointFeartures);
}

/**
 * Round coordinate to 250 (the resolution of the heatmap)
 * @param {number} num the coordinate to round
 * @returns {number} the input coordinate rounded to 250
 */
function roundCoordinate(num) {
  return Math.round(num / 250) * 250;
}

/**
 * Change the map layer of the heatmap to the right datatype,
 * update the UI to display the right title for the chart and the explanation of the data
 * and update the chart with the values for the selected datatype.
 * @param {string} dataType the new datatype, either "etap" or "swb"
 */
function handleDatatypeChange(dataType) {
  globalDataType = dataType;

  const waterPlantsText = document.getElementById('waterPlantsText');
  const waterGroundText = document.getElementById('waterGroundText');

  if (dataType === "etap") {
    wmtsSourceLayerURL = "ch.bafu.wald-wasserverfuegbarkeit_pflanzen";
    mapType = "pflanzen";
    $("#map").empty();

    if (document.getElementById("span_Graph")) {
      document.getElementById("span_Graph").innerHTML = translator.get("waterPlantsDiagramTitle");
    }

    $("#etap-description").show();
    $("#etap-key").show();

    $("#swb-description").hide();
    $("#swb-key").hide();

    waterPlantsText.classList.add('black-text');
    waterGroundText.classList.remove('black-text');

  } else {
    wmtsSourceLayerURL = "ch.bafu.wald-wasserverfuegbarkeit_boden";
    mapType = "boden";
    $("#map").empty();

    if (document.getElementById("span_Graph")) {
      document.getElementById("span_Graph").innerHTML = translator.get("waterGroundDiagramTitle");
    }

    $("#etap-description").hide();
    $("#etap-key").hide();

    $("#swb-description").show();
    $("#swb-key").show();
    waterPlantsText.classList.remove('black-text');
    waterGroundText.classList.add('black-text');
  }



  fetchGeoTiffs().then((result) => {
    geoTiffs = result;
    
    if (chart != undefined) {
      chartCoordinates = chart.coordinates;
      console.log(chart, chartCoordinates)
  
      let promise = Promise.resolve();
  
      chartCoordinates.forEach((coordinate, index) => {
        if (index === 0) {
          chart = new Chart(
            coordinate[0],
            coordinate[1],
            globalDataType,
            geoTiffs,
            firstYear,
            yearCount,
            changeYearInChart,
            displayPointsOnMap,
            pushURL
          );
          promise = promise.then(() => chart.build());
        } else {
          promise = promise.then(() => chart.addCoordinates(coordinate[0], coordinate[1]));
        }
      });
    }
  });

  buildMap();
  pushURL();
}

/**
 * Handles click on the year slider.
 * Note: If the animation is playing, it needs to be stopped to set the year and then restarted.
 */
function handleYearSliderClicked() {
  if (activeAutoplay === true) {
    stopAnimation();

    let yearSlider = $("#yearSlider");
    globalYear = yearSlider.val();

    startAnimation();
  }
}

/**
 * Handle year change from clicking on the chart
 * @param {number} year
 */
function changeYearInChart(year) {
  if (activeAutoplay === true) {
    stopAnimation();
  }

  let yearSlider = $("#yearSlider");
  yearSlider.val(year).change();
  yearSlider.attr("value", year);
  yearSlider.val(year).change();

  updateYear(wmtsSource, year);
  $("#span_year").html(year);

  if (activeAutoplay === true) {
    activeAutoplay = false;
    toggleAnimation();
  }
}


function toggleAnimation() {
  if (activeAutoplay === true) {
    stopAnimation();    

    // toggle button
    $("#btn_animation").removeClass("fa fa-pause").addClass("fa fa-play");
    activeAutoplay = false;
  } else {
    startAnimation();    

    // toggle button
    $("#btn_animation").removeClass("fa fa-play").addClass("fa fa-pause");
    activeAutoplay = true;
  }
}

/**
 * Start the year animation.
 */
function startAnimation() {
  const yearSlider = $("#yearSlider");
  const endValue = firstYear + yearCount - 1;

  yearSlider.val(globalYear).change();

  autoInterval = setInterval(function () {
    if (globalYear < endValue) {
      globalYear++;
    } else {
      globalYear = firstYear;
    }

    yearSlider.attr("value", globalYear);
    yearSlider.val(globalYear).change();
    updateYear(wmtsSource, globalYear);
    $("#span_year").html(globalYear);
    pushURL();
  }, 2000);
}

/**
 * Stop the year animation
 */
function stopAnimation() {
  clearInterval(autoInterval);
}

/**
 * Generate URL params representing the current state.
 * @returns {string} the url params representing the current state
 */
function generateURLParams() {
  let searchParams = new URLSearchParams();

  globalYear = $("#span_year").html();

  globalMapX = map.getView().getCenter()[0];
  globalMapY = map.getView().getCenter()[1];
  globalZoom = map.getView().getZoom();

  if (document.getElementById("data_etap").checked) {
    globalDataType = "etap";
  } else {
    globalDataType = "swb";
  }

  searchParams.set("year", globalYear);
  searchParams.set("mapX", globalMapX);
  searchParams.set("mapY", globalMapY);
  searchParams.set("zoom", globalZoom);
  searchParams.set("data", globalDataType);

  searchParams.set("lang", lang);

  if (chart != undefined) {
    searchParams = new URLSearchParams({
      ...Object.fromEntries(searchParams),
      ...Object.fromEntries(chart.chartUrlParams),
    });
  }

  return searchParams.toString();
}

/**
 * Show link to current state and copy it to clipboard.
 */
function share() {
  let link;
  const baseUrl = `${location.protocol}//${location.host}${location.pathname}`;
  const parameter = "?" + generateURLParams();

  link = baseUrl + parameter;
  // Show link field
  $("#sharedlink").val(link).removeAttr("hidden");

  navigator.clipboard.writeText(link);
  // Display notification
  M.toast({ html: "Link kopiert", classes: "rounded" });
}

/**
 * Reads the URL and sets the state accordingly.
 * Current year, map zoom and position, datatype, language and chart points are set.
 * @returns {Array<Array<number>>} 
 */
function readURL() {
  const searchParams = new URLSearchParams(location.search);

  if (searchParams.has("year") && searchParams.get("year")) {
    globalYear = searchParams.get("year");
  }

  if (searchParams.has("lang")) {
    lang = searchParams.get("lang");
  }

  if (searchParams.has("mapX")) {
    let x = parseFloat(searchParams.get("mapX"));
    if (!Number.isNaN(x)) {
      globalMapX = x;
    }
  }

  if (searchParams.has("mapY")) {
    let y = parseFloat(searchParams.get("mapY"));
    if (!Number.isNaN(y)) {
      globalMapY = y;
    }
  }

  if (searchParams.has("zoom")) {
    let zoom = parseFloat(searchParams.get("zoom"));
    if (!Number.isNaN(zoom)) {
      globalZoom = zoom;
    }
  }

  if (searchParams.has("data")) {
    globalDataType = searchParams.get("data");

    if (globalDataType === "etap") {
      $("#data_etap").prop("checked", true);
    } else if (globalDataType === "swb") {
      $("#data_swb").prop("checked", true);
    }
  }

  let i = 0;
  const chartCoordinates = [];
  while (searchParams.has(`chart${i}`)) {
    chartCoordinates.push(searchParams.get(`chart${i}`).split(","));

    i++;
  }

  // Set map view
  if (map && map.getView()) {
    const view = map.getView();
    view.setCenter([Math.round(globalMapX), Math.round(globalMapY)]);
    view.setZoom(Math.round(globalZoom * 100) / 100);
  }

  return chartCoordinates;
}

/**
 * Push the current state to the URL history.
 */
function pushURL() {
  const baseUrl = `${location.protocol}//${location.host}${location.pathname}`;
  const parameter = "?" + generateURLParams();

  const url = baseUrl + parameter;

  window.history.pushState({}, window.title, url);
}

/**
 * Set the new language and translate page.
 * @param {string} newLang the new language
 */
function changeLang(newLang) {
  lang = newLang;
  translator.lang(lang);
  document.title = translator.get("title");
  pushURL();
}

/**
 * Fetches the stac api infos for the currently set datatype.
 * @returns {Promise} promise of the api call.
 */
async function fetchStac() {
  return $.get(
    `https://data.geo.admin.ch/api/stac/v0.9/collections/ch.bafu.wald-wasserverfuegbarkeit_boden`,
    (result) => {
      stacGround = result;
    }
  ).then(() =>
    $.get(
      `https://data.geo.admin.ch/api/stac/v0.9/collections/ch.bafu.wald-wasserverfuegbarkeit_pflanzen`,
      (result) => {
        stacPlants = result;
      }
    )
  );
}

/**
 * Fetches geoTIFFs for all years for the currently set datatype.
 * @returns {Promise<Array<module:geotiffimage~GeoTIFFImage>>} a promise with all promises from all requests for every year
 */
async function fetchGeoTiffs() {
  return Promise.all(
    [...Array(yearCount).keys()].map((i) =>
      GeoTIFF.fromUrl(geoTiffUrl(firstYear + i)).then((gT) => gT.getImage())
    )
  );
}

$(document).ready(function () {
  $(".parallax").parallax();
  $(".modal").modal();

  const chartCoordinates = readURL();
  translator = $("body").translate({ lang: lang, t: translations });

  fetchStac()
    .then(() => {
      countYears();
      handleDatatypeChange(globalDataType);
    })
    .then(() => fetchGeoTiffs())
    .then((result) => {
      geoTiffs = result;

      let promise = Promise.resolve();

      chartCoordinates.forEach((coordinate, index) => {
        if (index === 0) {
          chart = new Chart(
            coordinate[0],
            coordinate[1],
            globalDataType,
            geoTiffs,
            firstYear,
            yearCount,
            changeYearInChart,
            displayPointsOnMap,
            pushURL
          );
          promise = promise.then(() => chart.build());
        } else {
          promise = promise.then(() => chart.addCoordinates(coordinate[0], coordinate[1]));
        }
      });

      return promise;
    })
    .then(() => {
      $("input").attr({ max: firstYear + yearCount - 1 });
      pushURL();
    });
});
