/**
 *
 */
class Chart {
  LOCALITY_API_BASE_URL =
    "https://api3.geo.admin.ch/rest/services/api/MapServer/identify?geometryType=esriGeometryPoint&tolerance=0&layers=all:ch.swisstopo.swissboundaries3d-gemeinde-flaeche.fill&returnGeometry=false&sr=2056&limit=1";
  coordinates = [];
  localityNames = [];
  highlightYear;
  dataType;
  chart;
  geoTiffs;
  firstYear;
  yearCount;
  changeYearInChart;
  displayPointsOnMap;
  pushUrl;

  /**
   * 
   * @param {number} x the x coordinate of the first data point
   * @param {number} y the y coordinate of the first data point
   * @param {string} dataType either etap or swb, used to determine which data set we should show
   * @param {Array<module:geotiffimage~GeoTIFFImage>} geoTiffs an array of geoTiffs with each having the data for one year
   * @param {number} firstYear the first year we have data for
   * @param {number} yearCount the number of years we have data for
   * @param {function} changeYearInChart callback function to handle changing years by clicking in the chart
   * @param {function} displayPointsOnMap callback function to draw the coordinate dots on the map
   * @param {function} pushUrl callback function to push the url to the history
   */
  constructor(
    x,
    y,
    dataType,
    geoTiffs,
    firstYear,
    yearCount,
    changeYearInChart,
    displayPointsOnMap,
    pushUrl
  ) {
    this.coordinates = [[x, y]];
    this.dataType = dataType;
    this.geoTiffs = geoTiffs;
    this.firstYear = firstYear;
    this.yearCount = yearCount;
    this.changeYearInChart = changeYearInChart;
    this.displayPointsOnMap = displayPointsOnMap;
    this.pushUrl = pushUrl;
  }

  /**
   * Render the chart with the first coordinate set
   * @returns {Promise}
   */
  async build() {
    this.pushUrl();
    return this.fetchLocalityName([this.coordinates[0][0], this.coordinates[0][1]])
      .then(() => this.drawInitialChart(this.coordinates[0]))
      .then(() => this.displayChartLocationsOnMap());
  }

  /**
   * The the county name of a specific point
   * @param {Array<number>} coordinates the coordinates of the point, where the first element is the x coordinate and the second element the y coordinate
   * @returns {Promise}
   */
  async fetchLocalityName(coordinates) {
    const x = coordinates[0];
    const y = coordinates[1];
    const localityApiUrl = `
    ${this.LOCALITY_API_BASE_URL}&lang=${lang}&geometry=${x},${y}&mapExtent=${x},${y},${x},${y}&imageDisplay=500,600,96`;

    return $.get(localityApiUrl).then((data) => {
      const attributes = data.results[0].attributes;

      // Don't add canton to localites which already include the canton
      const name =
        attributes.gemname.match(/\([A-Z]{2}\)$/g) != null
          ? attributes.gemname
          : `${attributes.gemname} (${attributes.kanton})`;

      this.localityNames.push(name);
    });
  }

  /**
   * Add a new data series for the given coordinates to the chart
   * @param {number} x the x coordinate
   * @param {number} y the y coordinate
   * @returns {Promise}
   */
  async addCoordinates(x, y) {
    this.coordinates.push([x, y]);
    this.pushUrl();

    return this.fetchLocalityName([x, y])
      .then(() => this.getDataForCoordinates(x, y))
      .then((data) => {
        // Normalize values between 0 and 1 if datatype is etap
        const dataArray = data.map((value) => (this.dataType === "etap" ? value / 100 : value));

        this.chart.load({
          columns: [["" + this.localityNames[this.localityNames.length - 1] + "", ...dataArray]],
        });
      })
      .then(() => this.displayChartLocationsOnMap());
  }

  /**
   * Get the data for a specific point from the geoTIFFs.
   * There is some maths needed to convert the coordinates to a pixel.
   * @param {number} x the x coordinate
   * @param {number} y the y coordinate
   * @returns {Promise}
   */
  async getDataForCoordinates(x, y) {
    const bbox = this.geoTiffs[0].getBoundingBox();
    const pixelWidth = this.geoTiffs[0].getWidth();
    const pixelHeight = this.geoTiffs[0].getHeight();
    const bboxWidth = bbox[2] - bbox[0];
    const bboxHeight = bbox[3] - bbox[1];
    const widthPct = (x - bbox[0]) / bboxWidth;
    const heightPct = (y - bbox[1]) / bboxHeight;
    const xPx = Math.floor(pixelWidth * widthPct);
    const yPx = Math.floor(pixelHeight * (1 - heightPct));
    const window = [xPx, yPx, xPx + 1, yPx + 1];

    return Promise.all(this.geoTiffs.map((geoTiff) => geoTiff.readRasters({ window })));
  }

  /**
   * Initializes the c3 chart and populate it with the first data
   * @param {Array<number>} coordinates the first element is the x coordinate and the second element the y coordinate
   * @returns {Promise}
   */
  async drawInitialChart(coordinates) {
    const that = this;
    return this.getDataForCoordinates(coordinates[0], coordinates[1]).then(function (data) {
      // Normalize values between 0 and 1 if datatype is etap
      const dataArray = data.map((value) => (that.dataType === "etap" ? value / 100 : value));
      const allYears = [...Array(that.yearCount).keys()].map((i) => `${i + that.firstYear}-01-01`);

      that.displayChartLocationsOnMap(coordinates);

      that.chart = c3.generate({
        data: {
          x: "x",
          columns: [
            ["x", ...allYears],
            [that.localityNames[0], ...dataArray],
          ],
          onclick: function (e) {
            that.changeYearInChart(e.index + that.firstYear);
          },
        },
        axis: {
          x: {
            type: "timeseries",
            tick: {
              format: "%Y",
            },
          },
          y: {
            tick: {
              format: (d) => (that.dataType === "swb" ? `${d} mm` : d),
            },
          },
        },
        grid: {
          y: {
            lines: that.dataType === "etap" ? [{ value: 0.8, text: "Kritischer Wert" }] : [],
          },
        },
      });
    });
  }

  /**
   * Display the chart locations on the map.
   * Ensure the dots on the map have the same colors as the lines of the chart.
   */
  displayChartLocationsOnMap() {
    const points = [];
    const colors = [];
    const that = this;

    this.coordinates.forEach(function (coordinate, index) {
      points.push([coordinate[0], coordinate[1]]);
      const name = that.localityNames[index].replace(/\s/g, "-");

      colors.push($($("." + $.escapeSelector(`c3-line-${name}`))[0]).css("stroke"));
    });

    this.displayPointsOnMap(points, colors);
  }

  /**
   * Highlight the year currently shown on the map on the chart
   * @param {number} circlenumber
   */
  colorYearInChart(circlenumber) {
    // reset all dots to default
    $(".c3-circle").removeClass("currentDataYear").attr("r", "2.5");

    // highlight dots of currently selected year
    $(".c3-circle-" + circlenumber)
      .addClass("currentDataYear")
      .attr("r", "4.375");
  }

  get chartUrlParams() {
    const searchParams = new URLSearchParams();

    this.coordinates.forEach((coordinate, index) => {
      searchParams.set(`chart${index}`, `${coordinate[0]},${coordinate[1]}`);
    });

    return searchParams;
  }
}
